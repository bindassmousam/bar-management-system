package product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import login.TxtCompare;



public class up_prod extends login.Connect {
	public up_prod(String id){
		
		
		JFrame jf=new JFrame("Sales Rate");
		jf.setSize(350,170);
		jf.setLocationRelativeTo(null);
		jf.setLayout(null);
		
		
		JLabel user=new JLabel("New Sales Rate(Rs):");
		user.setBounds(10,15,150,20);
		jf.add(user);
		
		final JTextField use1=new JTextField();
		use1.setBounds(135,15,150,20);
		jf.add(use1);
		
		
		JButton btn2=new JButton("Update");
		btn2.setBounds(75,60,145,30);
		btn2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
                                String rate=use1.getText();
				if(!TxtCompare.compare(rate, "N")){
                                    JOptionPane.showMessageDialog(null, "Please insert valid Rate.");
                                    return;
                                }
				String sql="UPDATE `product` SET sales_rate='"+rate+"' WHERE `Product`.`Product_ID` = '"+id+"' ";       
				
				append(sql,"Updated");
		
                                jf.dispose();
			}
		});
                jf.add(btn2);
                
                jf.setVisible(true);
		
	}
}
