/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package purchase;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Stack;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import static javax.swing.JTable.AUTO_RESIZE_OFF;
import javax.swing.JTextField;
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;
import login.Connect;
import login.TxtCompare;

/**
 *
 * @author Bindass Mousam
 */
@SuppressWarnings("OverridableMethodCallInConstructor")
public class npurch extends JFrame{
    JButton jButton1 = new JButton();
    JButton jButton2 = new JButton();
    JButton jButton3 = new JButton();
    JButton jButton4 = new JButton();
    JComboBox<String> jComboBox1 = new JComboBox<>();
    JComboBox<String> jComboBox2 = new JComboBox<>();
    JComboBox<String> jComboBox3 = new JComboBox<>();
    JLabel jLabel1 = new JLabel();
    JLabel jLabel10 = new JLabel();
    JLabel jLabel2 = new JLabel();
    JLabel jLabel3 = new JLabel();
    JLabel jLabel4 = new JLabel();
    JLabel jLabel5 = new JLabel();
    JLabel jLabel6 = new JLabel();
    JLabel jLabel7 = new JLabel();
    JLabel jLabel8 = new JLabel();
    JLabel jLabel9 = new JLabel();
    JScrollPane jScrollPane1 = new JScrollPane();
    JSeparator jSeparator1 = new JSeparator();
    JTable jTable1 = new JTable();
    JComboBox jTextField1 = new JComboBox();
    JTextField jTextField2 = new JTextField();
    JTextField jTextField3 = new JTextField();
    JTextField jTextField4 = new JTextField();
    JTextField jTextField5 = new JTextField();
    JTextField jTextField6 = new JTextField();
    JTextField jTextField7 = new JTextField();
    DefaultTableModel tablemodel;
    double total = 0;
    Connect conn;
    Stack<String[]> itemStack = new Stack<>();
    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch", "static-access"})
    public npurch(){
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("Purchase");
        setLayout(null);
        setSize(862, 502);
        setLocationRelativeTo(null);
        setResizable(false);
        
        conn = new Connect();

        jLabel1.setText("Supplier:");
        add(jLabel1);
        jLabel1.setBounds(10, 59, 66, 14);

        add(jTextField1);
        jTextField1.setBounds(94, 56, 191, 20);
        try{
                String sql="SELECT Supplier_id,Sname FROM supplier";
                ResultSet rs=conn.display(sql);
                while(rs.next()){
                    jTextField1.addItem(rs.getString(1)+" | "+rs.getString(2)); 
                }
            }catch(Exception e){		
            }

        jLabel2.setText("Category:");
        add(jLabel2);
        jLabel2.setBounds(10, 85, 59, 14);

        jComboBox1.addItem("Select Category");
            try{
                String sql="SELECT `Category_Id`, `Cname` FROM `category` ORDER BY Cname ";
                ResultSet rs=conn.display(sql);
                while(rs.next()){
                    jComboBox1.addItem(rs.getString(1)+" | "+rs.getString(2)); 
                }
            }catch(Exception e){		
            }
                
        jComboBox1.addActionListener((ActionEvent evt) -> {
            jComboBox2.removeAllItems();
            if(jComboBox1.getSelectedItem().equals("Select Category")){
                jComboBox2.setEnabled(false);
                jComboBox3.setEnabled(false);
            }
            else{
                jComboBox2.setEnabled(true);
                String Selection1 = jComboBox1.getSelectedItem().toString();
                String id1;
                id1=Selection1.substring(0,Selection1.indexOf(" | "));
                ResultSet rs = conn.display("Select * From sub_category where Category_ID="+id1+" Order By subName");
                try {
                    while(rs.next()){
                        jComboBox2.addItem(rs.getString("subName"));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
        add(jComboBox1);
        jComboBox1.setBounds(94, 82, 192, 20);

        jLabel3.setText("Sub Category:");
        add(jLabel3);
        jLabel3.setBounds(10, 108, 80, 14);

        jComboBox2.setEnabled(false);
        jComboBox2.addActionListener((ActionEvent evt) -> {
            jComboBox3.removeAllItems();
            if(jComboBox2.getSelectedIndex()<0){
                jComboBox3.setEnabled(false);
            }
            else{
                jComboBox3.setEnabled(true);
                String Selection1 = jComboBox1.getSelectedItem().toString();
                String id1;
                id1=Selection1.substring(0,Selection1.indexOf(" | "));
                String Selection2 = jComboBox2.getSelectedItem().toString();
                ResultSet rs = conn.display("Select Product_Id,Product_name From product where Category='"+id1+"' AND Sub_Category='"+Selection2+"' Order By Product_name");
                try {
                    while(rs.next()){
                        jComboBox3.addItem(rs.getString(1)+" | "+rs.getString(2));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
        add(jComboBox2);
        jComboBox2.setBounds(94, 108, 192, 20);

        jLabel4.setText("Product:");
        add(jLabel4);
        jLabel4.setBounds(10, 137, 51, 14);

        jComboBox3.setEnabled(false);
        add(jComboBox3);
        jComboBox3.setBounds(94, 134, 192, 20);

        jLabel5.setText("Rate:");
        add(jLabel5);
        jLabel5.setBounds(10, 163, 37, 14);

        add(jTextField2);
        jTextField2.setBounds(94, 160, 192, 20);

        jLabel6.setText("Quantity:");
        add(jLabel6);
        jLabel6.setBounds(10, 189, 56, 14);

        add(jTextField3);
        jTextField3.setBounds(94, 186, 192, 20);

        jButton1.setText("Add");
        jButton1.addActionListener((ActionEvent e) -> {
            if(jTextField1.getSelectedIndex()<0)
                JOptionPane.showMessageDialog(null, "Input Suppliar.");
            else if(jComboBox1.getSelectedIndex()<1)
                JOptionPane.showMessageDialog(null, "Input Category.");
            else if(jComboBox2.getSelectedIndex()<0)
                JOptionPane.showMessageDialog(null, "Input Sub Category.");
            else if(jComboBox3.getSelectedIndex()<0)
                JOptionPane.showMessageDialog(null, "Input Product.");
            else if(jTextField2.getText().length()<=0)
                JOptionPane.showMessageDialog(null, "Input rate");
            else if(!TxtCompare.compare(jTextField2.getText(), "F"))
                JOptionPane.showMessageDialog(null, "Input Valid rate.");
            else if(jTextField3.getText().length()<=0)
                JOptionPane.showMessageDialog(null, "Input quantity");
            else if(!TxtCompare.compare(jTextField3.getText(), "N"))
                JOptionPane.showMessageDialog(null, "Input Valid quantity.");
            else{
                String a = jComboBox3.getSelectedItem().toString();
                String b = jTextField2.getText();
                String c = jTextField3.getText();
                String d = ((double)Double.parseDouble(b)*Integer.parseInt(c))+"";
                
                tablemodel.addRow(new String[]{a,b,c,d});
                itemStack.push(new String[]{a,c,b});
                total = total + (double)Double.parseDouble(d);
                jTextField1.setEnabled(false);
                calculate();
                clearEntry();
            }
        });
        add(jButton1);
        jButton1.setBounds(105, 217, 78, 23);

        jButton2.setText("Clear");
        jButton2.addActionListener((ActionEvent evt) -> {
            clearEntry();
        });
        add(jButton2);
        jButton2.setBounds(105, 246, 78, 23);

        tablemodel = new DefaultTableModel(){
          @Override
          public boolean isCellEditable(int row,int col){
              return false;
          }
        };
        jTable1.setModel(tablemodel);
        String[] cols = {"Name","Rate","Quantity","Total"};
        tablemodel.setColumnIdentifiers(cols);
        jTable1.setEnabled(true);
        jTable1.setShowGrid(true);
        
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(240);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
        jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
        
        jTable1.setAutoResizeMode(AUTO_RESIZE_OFF);
        jTable1.getTableHeader().setResizingAllowed(false);
        
        jScrollPane1.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setViewportView(jTable1);

        add(jScrollPane1);
        jScrollPane1.setBounds(296, 11, 540, 281);

        jLabel7.setText("Total:");
        add(jLabel7);
        jLabel7.setBounds(640, 316, 60, 14);

        jTextField4.setEditable(false);
        add(jTextField4);
        jTextField4.setBounds(715, 313, 121, 20);

        jLabel8.setText("Vat:");
        add(jLabel8);
        jLabel8.setBounds(640, 342, 60, 14);

        jTextField5.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if( TxtCompare.compare(jTextField5.getText(), "F") || TxtCompare.compare(jTextField5.getText(), "N"))
                    calculate();
                else if(jTextField5.getText().lastIndexOf(".")==jTextField5.getText().indexOf(".") && jTextField5.getText().indexOf(".")==jTextField5.getText().length()-1 && jTextField5.getText().length()!=0)
                    calculate();
                else {
                    jTextField5.setText("");
                    calculate();
                }
            }
        });
        add(jTextField5);
        jTextField5.setBounds(715, 339, 121, 20);

        jLabel9.setText("Discount:");
        add(jLabel9);
        jLabel9.setBounds(640, 368, 60, 14);

        jTextField6.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if( TxtCompare.compare(jTextField6.getText(), "F") || TxtCompare.compare(jTextField6.getText(), "N"))
                    calculate();
                else if(jTextField6.getText().lastIndexOf(".")==jTextField6.getText().indexOf(".") && jTextField6.getText().indexOf(".")==jTextField6.getText().length()-1 && jTextField6.getText().length()!=0)
                    calculate();
                else {
                    jTextField6.setText("");
                    calculate();
                }
            }
        });
        add(jTextField6);
        jTextField6.setBounds(715, 365, 121, 20);

        jLabel10.setText("Grand Total:");
        add(jLabel10);
        jLabel10.setBounds(640, 394, 70, 14);

        jTextField7.setEditable(false);
        add(jTextField7);
        jTextField7.setBounds(715, 391, 121, 20);

        jButton3.setText("Submit");
        jButton3.addActionListener((ActionEvent e) -> {
            submit();
        });
        add(jButton3);
        jButton3.setBounds(761, 429, 75, 23);

        jButton4.setText("Close");
        jButton4.addActionListener((ActionEvent e) -> {
            dispose();
        });
        add(jButton4);
        jButton4.setBounds(10, 429, 75, 23);
        add(jSeparator1);
        jSeparator1.setBounds(10, 303, 826, 4);

        setVisible(true);
    }
    private void calculate() {
        jTextField4.setText(total+"");
        double vat = 0;
        if(jTextField5.getText().length()>0)
            vat = (double) Double.parseDouble(jTextField5.getText());
        double discount = 0;
        if(jTextField6.getText().length()>0)
            discount = (double) Double.parseDouble(jTextField6.getText());
        if(discount>total){
            jTextField6.setText("0");
            discount = 0;
        }
        double GT = total+vat-discount;
        jTextField7.setText(GT+"");
    }
    private void clearEntry() {
        jComboBox1.setSelectedIndex(0);
        jTextField2.setText("");
        jTextField3.setText("");
    }
    @SuppressWarnings({"CallToPrintStackTrace", "static-access", "UseSpecificCatch"})
    private void submit() {
        try{
            conn.s.execute("CREATE TABLE IF NOT EXISTS stock("
                    + "pid INT UNIQUE,"
                    + "quantity INT DEFAULT 0,"
                    + "FOREIGN KEY(pid) REFERENCES product(Product_Id))"
            );
            conn.s.execute("CREATE TABLE IF NOT EXISTS purchase("
                    + "id INT PRIMARY KEY AUTO_INCREMENT,"
                    + "purchase_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
                    + "suppliar_id INT NOT NULL,"
                    + "pid INT NOT NULL,"
                    + "quantity INT DEFAULT 0,"
                    + "rate DOUBLE DEFAULT 0.0,"
                    + "FOREIGN KEY(pid) REFERENCES product(Product_Id))"
            );
            boolean flag = false;
            for(int i=0;i<itemStack.size();i++){
                String[] temp = itemStack.get(i);
                String id = temp[0].substring(0,temp[0].indexOf(" | "));
                String qty = temp[1];
                String rate = temp[2];
                String supp = jTextField1.getSelectedItem().toString();
                supp = supp.substring(0,supp.indexOf(" | "));
                conn.s.execute("INSERT INTO purchase(suppliar_id,pid,quantity,rate) VALUES('"+supp+"','"+id+"','"+qty+"','"+rate+"')");
                ResultSet rs = conn.s.executeQuery("SELECT * FROM stock WHERE pid='"+id+"'");
                if(rs.next()){
                    qty = (Integer.parseInt(qty)+rs.getInt("quantity"))+"";
                    conn.s.execute("UPDATE stock SET quantity='"+qty+"' WHERE pid='"+id+"'");
                }
                else{
                    conn.s.execute("INSERT INTO stock(pid,quantity) VALUES('"+id+"','"+qty+"')");
                }
                flag=true;
            }
            if(flag){
                JOptionPane.showMessageDialog(null, "Inserted to stock.");
                dispose();
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}