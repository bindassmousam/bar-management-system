package category;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;



public class disp_cate extends login.Connect {
public disp_cate(){
		JFrame frame=new JFrame();
		 frame.setTitle("Category");
		
		  
	        String[] columnNames = { "Category Id", "CName"};
	        final DefaultTableModel model=new DefaultTableModel();
			final JTable view = new JTable();
			
			//table	
			view.setModel(model);
                        
                        view.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                            @Override
                            public void valueChanged(ListSelectionEvent event) {
                                    // print first column value from selected row
                                    
                                    System.out.println(view.getSelectedRow());
                            }
                        });
                        
                        
			model.setColumnIdentifiers(columnNames);
			
		
		view.setRowHeight(25);
		view.getColumnModel().getColumn(0).setPreferredWidth(5);
		view.getColumnModel().getColumn(1).setPreferredWidth(5);
		
		view.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		final JScrollPane sp=new JScrollPane(view);
		sp.setBounds(40,40,700,200);
		
		try {
			String sql="SELECT * from category";
			ResultSet rs=display(sql);
                        
			
			while(rs.next()){
				model.addRow(new String[]{rs.getString(1),rs.getString(2)});
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		JLabel search=new JLabel("Search");
		search.setBounds(10,10,50,20);
		frame.add(search);
		
		final JTextField Search=new JTextField();
		Search.setBounds(100,10,300,20);
		frame.add(Search);
		Search.addKeyListener(new KeyAdapter() {
	        public void keyReleased(KeyEvent e) {  
	        	try {
	        	String sql="SELECT * FROM `category` WHERE `CName` LIKE '"+Search.getText().toString()+"%'";
	            ResultSet rs=display(sql);
	            sp.repaint();
	        	model.getDataVector().removeAllElements();
	        	while(rs.next()){
	            	model.addRow(new Object[]{rs.getString(1).toString(),rs.getString(2).toString()});
					}
	        	} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            
	        	}
	        });
		
		
		JButton Add= new JButton("Add New");
		Add.setBounds(440,280,100,30);
		frame.add(Add);
		
		Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
                
			try {
				new ncate();
				
			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			}
		});
	
		
		JButton Update= new JButton("Update");
		Update.setBounds(200,280,100,30);
		frame.add(Update);
		Update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
				
				    String ID = null;  
				    String Name = null;  
				    String Sub_cate = null;  
				    int[] row = view.getSelectedRows();    
	                for (int i = 0; i < row.length; i++) {   
	                    ID = (String) view.getValueAt(row[i],0);  
	                    Name = (String) view.getValueAt(row[i], 1);  
	                    
		                    
	                } 
				
				
				
    		new up_cate(ID,Name);	
			}
		});
		
		
		
		
		JButton Del= new JButton("Delete");
		Del.setBounds(320,280,100,30);
		frame.add(Del);
		Del.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
				 String ID = null;  
				    int[] row = view.getSelectedRows();    
	                for (int i = 0; i < row.length; i++) {   
	                    ID = (String) view.getValueAt(row[i],0);  
	                 } 
    		new del_cate(ID);
			}
		});
	
		 
		
		
		
		
		
		frame.setSize(780,400);
		frame.setLocationRelativeTo(null);
		frame.setLayout(null);
	
		frame.add(sp);
		
		frame.setVisible(true);
	}
}
