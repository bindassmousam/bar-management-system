package supplier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.*;

public class up_supp extends login.Connect {

	public up_supp(final String id) {

		getConnect();

		JFrame nsu = new JFrame("Supplier details");
		nsu.setSize(350, 320);
		nsu.setLocationRelativeTo(null);
		nsu.setLayout(null);

		JLabel user = new JLabel("Supplier Name:");
		user.setBounds(10, 10, 150, 20);
		nsu.add(user);

		final JTextField use1 = new JTextField();
		use1.setBounds(100, 10, 150, 20);
		nsu.add(use1);

		JLabel user4 = new JLabel("Address:");
		user4.setBounds(10, 40, 150, 20);
		nsu.add(user4);

		final JTextField use4 = new JTextField();
		use4.setBounds(100, 40, 150, 20);
		nsu.add(use4);

		JLabel user5 = new JLabel("Ph.No:");
		user5.setBounds(10, 70, 150, 20);
		nsu.add(user5);

		final JTextField use5 = new JTextField();
		use5.setBounds(100, 70, 150, 20);
		nsu.add(use5);

		JLabel user7 = new JLabel("Email:");
		user7.setBounds(10, 100, 150, 20);
		nsu.add(user7);

		final JTextField use6 = new JTextField();
		use6.setBounds(100, 100, 150, 20);
		nsu.add(use6);

		JLabel user8 = new JLabel("Vat/Pan No.");
		user8.setBounds(10, 130, 150, 20);
		nsu.add(user8);

		final JTextField use8 = new JTextField();
		use8.setBounds(100, 130, 150, 20);
		nsu.add(use8);

		JButton update = new JButton("Update");
		update.setBounds(40, 170, 80, 30);
		nsu.add(update);

		JButton clear = new JButton("CLEAR");
		clear.setBounds(140, 170, 80, 30);
		nsu.add(clear);

		try {

			String sql = "SELECT * FROM `supplier` WHERE `supplier`.`Supplier_id`='" + id + "'";
			ResultSet rs = display(sql);
			while (rs.next()) {
				use1.setText(rs.getString(2).toString());
				use4.setText(rs.getString(3).toString());
				use5.setText(rs.getString(4).toString());
				use6.setText(rs.getString(5).toString());
				use8.setText(rs.getString(6).toString());
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}

		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {

				 String name=use1.getText();
				 String add=use4.getText();
				 String con=use5.getText();
				 String mail=use6.getText();
				 String vat=use8.getText();

				String sql = "UPDATE `supplier` SET `Sname`='" + name + "',`Address`='" + add
						+ "',`Contact`='" + con + "',`Email`='" + mail + "',`Vat_Pan`='"
						+ vat + "' WHERE Supplier_id='" + id + "'";
				append(sql, "Updated");
                                nsu.dispose();
			}
		});
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				use1.setText("");
				use4.setText("");
				use5.setText("");
				use6.setText("");
				use8.setText("");
			}
		});
		//
		nsu.setVisible(true);
	}
}
