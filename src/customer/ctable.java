/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customer;

import java.sql.ResultSet;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import login.Connect;

/**
 *
 * @author Bindass 
 * 
 */
public class ctable extends JTable{
    @SuppressWarnings({"OverridableMethodCallInConstructor", "UseSpecificCatch"})
    public ctable(ResultSet rs,DefaultTableModel model){
        setModel(model);
        getColumnModel().getColumn(0).setPreferredWidth(5);
        getColumnModel().getColumn(1).setPreferredWidth(120);
        getColumnModel().getColumn(2).setPreferredWidth(100);
        getColumnModel().getColumn(3).setPreferredWidth(50);
        getColumnModel().getColumn(4).setPreferredWidth(10);
        getColumnModel().getColumn(5).setPreferredWidth(100);
        try{
            while(rs.next()){
                model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6)});
            }
       }catch(Exception E){
           System.out.println(E);
       }
    }
}