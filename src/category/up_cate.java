package category;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class up_cate extends login.Connect {

	up_cate(final String id,String name,String dis){
		final JFrame frame=new JFrame("Update Product Category");
		frame.setSize(380,260);
		frame.setLayout(null);
		frame.setLocationRelativeTo(null);
		
	
		JLabel cid=new JLabel("Category ID :");
		cid.setBounds(20, 10, 80, 20);
		cid.setHorizontalAlignment(JLabel.RIGHT);
		frame.add(cid);
		
		JTextField idtxt=new JTextField(id);
		idtxt.setBounds(120, 10, 200, 20);
		frame.add(idtxt);
		
		JLabel Category = new JLabel("Category :");
		Category.setHorizontalAlignment(JLabel.RIGHT);
		Category.setBounds(20,33,80,20);
		frame.add(Category);
		
		final JTextField catTxt = new JTextField(name);
		catTxt.setBounds(120,33,200,20);
		frame.add(catTxt);
		
		JLabel Dis = new JLabel("Description :");
		Dis.setHorizontalAlignment(JLabel.RIGHT);
		Dis.setBounds(20,56,80,20);
		frame.add(Dis);
		
		final JTextArea DisTxt = new JTextArea(dis);
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		DisTxt.setBounds(120,56,200,100);
		DisTxt.setBorder(BorderFactory.createCompoundBorder(border,
	    BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		DisTxt.setLineWrap(true);
		frame.add(DisTxt);
		
		JButton Insert=new JButton("UPDATE");
		Insert.setBounds(70,170,100,30);
		frame.add(Insert);
		
		Insert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				String name=catTxt.getText().toString();
				String des=DisTxt.getText().toString();
				String sql="UPDATE `category` SET `Category ID` = '"+id+"', `Category Name` = '"+name+"', `Description` = '"+des+"' WHERE `category`.`Category ID` = '"+id+"' ";
			append(sql,"Updated");	
			frame.dispose();
			}
		});
		
		JButton Clear=new JButton("CLEAR");
		Clear.setBounds(180,170,100,30);
		frame.add(Clear);
		
		
		frame.setVisible(true);
	}

    up_cate(String ID, String Name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
	