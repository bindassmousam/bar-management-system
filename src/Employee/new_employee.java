package Employee;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import login.Connect;
import login.TxtCompare;

   
public class new_employee extends Connect {
	int cid;
	public new_employee() {
		
	JFrame nview=new JFrame("Employee details");
	nview.setSize(400,300);
	nview.setLocationRelativeTo(null);
	nview.setLayout(null);
		
	
		JLabel user=new JLabel("Name:");
		user.setBounds(10,10,150,20);
		nview.add(user);
		
		final JTextField use1=new JTextField();
		use1.setBounds(90,10,150,20);
		nview.add(use1);
                
		
		JLabel user4=new JLabel("Address:");
		user4.setBounds(10,40,150,20);
		nview.add(user4);
		
		final JTextField use4=new JTextField();
		use4.setBounds(90,40,150,20);
		nview.add(use4);
		
		JLabel user5=new JLabel("Ph.No:");
		user5.setBounds(10,70,150,20);
		nview.add(user5);
		
		final JTextField use5=new JTextField();
		use5.setBounds(90,70,150,20);
		nview.add(use5);
		
		
		JLabel user6=new JLabel("Gender:");
		user6.setBounds(10,100,100,20);
		nview.add(user6);
		
		
		JRadioButton maleButton=new JRadioButton("Male");
		maleButton.setSelected(true);
		JRadioButton femaleButton=new JRadioButton("Female");
		JRadioButton otherButton=new JRadioButton("Other");
		final ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(maleButton);
		bgroup.add(femaleButton);
		bgroup.add(otherButton);
		nview.add(maleButton);
		maleButton.setBounds(90,100,60,20);
		maleButton.setActionCommand("Male");
		nview.add(femaleButton);
		femaleButton.setBounds(150,100,70,20);
		femaleButton.setActionCommand("Female");
		nview.add(otherButton);
		otherButton.setBounds(220,100,80,20);
		
		
		 JLabel user7=new JLabel("Post");
		user7.setBounds(10,130,150,20);
		nview.add(user7);
		
		final JTextField use6=new JTextField();
		use6.setBounds(90,130,200,20);
		nview.add(use6);				     				      				   		
		
		JButton btn2=new JButton("Insert");
		btn2.setBounds(90,170,100,40);
		btn2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				
			String name=use1.getText().trim();
			String add=use4.getText().trim();
			String gen = bgroup.getSelection().getActionCommand();	
			String con=use5.getText().trim();
                        String post=use6.getText().trim();
			 if(name.length()==0||add.length()==0||con.length()==0||post.length()==0)
				 JOptionPane.showMessageDialog(null, "Fields cannot be empty.", "Error", JOptionPane.ERROR_MESSAGE);
			 else if (!TxtCompare.compare(name, "Aas")) {
			    JOptionPane.showMessageDialog(null, "Please enter a valid character", "Error", JOptionPane.ERROR_MESSAGE);
                                        use1.requestFocus();
			    }  
				else if (!TxtCompare.compare(con,"N")) { 
		              
				 JOptionPane.showMessageDialog(null, "Please enter a valid number", "Error", JOptionPane.ERROR_MESSAGE);
                                    
                                 use5.requestFocus();
                                
                                }
				//String gen=bgroup.getSelection().;
				
			    
				else if(!TxtCompare.compare(post,"Aas")) {
				 JOptionPane.showMessageDialog(null, "Please enter a valid Email address", "Error", JOptionPane.ERROR_MESSAGE);
                                    use6.requestFocus();
		     }
			 
				 
			 else {
				String sql="INSERT INTO `employee`(`Name`, `Address`, `Gender`, `Contact_No`, `Position`) VALUES ('"+name+"', '"+add+"','"+gen+"','"+con+"','"+post+"')";          
				nview.dispose();
				append(sql,"Inserted");

				}
				
			
			}
			
		});
		nview.add(btn2);
		JButton clear=new JButton("Clear");
		clear.setBounds(210,170,100,40);
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				use1.setText("");
				use4.setText("");
				use5.setText("");
				use6.setText("");
				
			}
		});
		nview.add(clear);
		
		
		nview.setVisible(true);
	
}	
}
