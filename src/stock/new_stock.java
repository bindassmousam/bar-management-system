package stock;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.JTable.AUTO_RESIZE_OFF;
import javax.swing.JTextField;
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
import javax.swing.table.DefaultTableModel;
import login.Connect;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


@SuppressWarnings("OverridableMethodCallInConstructor")
public class new_stock extends Connect{
    String searchS="",catS="",scatS="",query="";
    DefaultTableModel tablemodel;
    JScrollPane jScrollPane1 = new JScrollPane();
    JTextField Search;
    JComboBox<String> cmb1 = new JComboBox<>();
    JComboBox<String> cmb2 = new JComboBox<>();
    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    Connect conn = new Connect();
    JTable view = new JTable();
    boolean flag=false;
    @SuppressWarnings({"CallToPrintStackTrace", "UseSpecificCatch"})
    public new_stock(){
        JFrame pf=new JFrame();
        pf.setTitle("Product");
 
        tablemodel = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row,int col){
                return false;
            }
        };

        view.setModel(tablemodel);
        String[] cols = {"SN.","Product Name","Stock"};
        tablemodel.setColumnIdentifiers(cols);
        view.setEnabled(true);
        view.setShowGrid(true);
        
        view.getColumnModel().getColumn(0).setPreferredWidth(50);
        view.getColumnModel().getColumn(1).setPreferredWidth(250);
        view.getColumnModel().getColumn(2).setPreferredWidth(100);
        
        view.setAutoResizeMode(AUTO_RESIZE_OFF);
        view.getTableHeader().setResizingAllowed(false);
        
        jScrollPane1.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setViewportView(view);
	jScrollPane1.setBounds(4,4,415,450);
 
        JLabel search=new JLabel("Search by Product name:");
        search.setBounds(425,15,150,30);
        pf.add(search);
	
        Search=new JTextField();
        Search.setBounds(575,15,155,30);
        pf.add(Search);
        Search.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                paintTable();
            }
        });
                        
        JLabel user6=new JLabel("Category:");
	user6.setBounds(425,60,150,30);
	pf.add(user6);

        cmb1.addItem("Select Category");
        try{
            String sql="SELECT `Category_Id`, `Cname` FROM `category` ORDER BY Cname ";
            ResultSet rs=display(sql);
            while(rs.next()){
                cmb1.addItem(rs.getString(1)+" | "+rs.getString(2)); 
            }
        }catch(Exception e){
        }
        cmb1.setSelectedIndex(0);
        String ca=cmb1.getSelectedItem().toString();
        if(ca.equals("Select Category"))
            cmb2.setEnabled(false);
	cmb1.setBounds(575,60,155,30);
        cmb1.addActionListener((ActionEvent e) -> {
            cmb2.removeAllItems();
            String cate=cmb1.getSelectedItem().toString();
            if(cate.equals("Select Category"))
                cmb2.setEnabled(false);
            else{
                cmb2.setEnabled(true);
                String id1;
                int temp=cate.indexOf(" | ");
                String catego=cate.substring(temp+3,cate.length());
                id1=cate.substring(0,temp);
                ResultSet re=display("Select * From sub_category where Category_ID="+id1+" Order By subName");
                try {
                    while(re.next()){
                        flag=true;
                        cmb2.addItem(re.getString(2));
                    }
                    flag=false;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                paintTable();
            }
        });
        pf.add(cmb1);
        JLabel user7=new JLabel("Sub Category:");
	user7.setBounds(425,100,150,30);
	pf.add(user7);

        cmb2.addActionListener((ActionEvent e) -> {
            if(!flag)
                paintTable();
        });
	cmb2.setBounds(575,100,155,30);		
        pf.add(cmb2);

        JButton btn = new JButton("Print");
        btn.addActionListener((ActionEvent e) -> {
            try {
                HashMap param=new HashMap();
                String jrxmlLoc = getClass().getResource("/stock/TotalStock.jrxml").toString();
                jrxmlLoc = jrxmlLoc.substring(jrxmlLoc.indexOf(":")+1);
                JasperReport jr = JasperCompileManager.compileReport(JRXmlLoader.load(new FileInputStream(new File(jrxmlLoc))));
                param.put("param1", "%"+searchS+"%");
                param.put("param2", "%"+catS+"%");
                param.put("param3", "%"+scatS+"%");
                @SuppressWarnings("static-access")
                JasperPrint j = JasperFillManager.fillReport(jr,param,conn.conn);
                JasperViewer.viewReport(j,false);
            } catch (Exception ex) {
                System.out.println(ex);
                JOptionPane.showMessageDialog(null, ex);
            }
        });
        btn.setBounds(565, 140, 80, 20);
        pf.add(btn);
        paintTable();
        
        pf.setSize(755,500);
        pf.setLocationRelativeTo(null);
        pf.setLayout(null);
        pf.add(jScrollPane1);
	
        pf.setVisible(true);
    }
    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    private void paintTable() {
        tablemodel.getDataVector().removeAllElements();
        try{
            searchS = Search.getText();
        }catch(NullPointerException e){
            searchS = "";
        }
        if(cmb1.getSelectedItem().toString().equals("Select Category"))
            catS = "";
        else
            catS = cmb1.getSelectedItem().toString().substring(0,cmb1.getSelectedItem().toString().indexOf(" | "));
        if(cmb2.getSelectedIndex()<0)
            scatS = "";
        else
            scatS = cmb2.getSelectedItem().toString();
        query = "SELECT product.Product_name,stock.quantity FROM product,stock WHERE product.Product_Id=stock.pid AND product.Product_name LIKE '%"+searchS+"%' AND product.Category LIKE '%"+catS+"%' AND product.Sub_Category LIKE '%"+scatS+"%' ORDER BY product.Product_name";
        try{
            int count=1;
            @SuppressWarnings("static-access")
            ResultSet rs = conn.s.executeQuery(query);
            while(rs.next()){
                String a = count+"";
                String b = rs.getString(1);
                String c = rs.getInt(2)+"";
                tablemodel.addRow(new String[]{a,b,c});
                count++;
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        view.repaint();
        view.revalidate();
        jScrollPane1.repaint();
        jScrollPane1.revalidate();
    }
}
