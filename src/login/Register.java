package login;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Register extends Connect{
	
	 Register() {
		 

		  
	
		 final JFrame jf = new JFrame("Register");
		
			jf.setLayout(null);
			jf.setSize(390,350);
			
			jf.setLocationRelativeTo(null);
			jf.setBackground(Color.pink);
			
			
		
	   
	        JLabel lbl=new JLabel("Usertype:");  
	        lbl.setForeground(Color.red);
	        lbl.setLayout(null);
	        lbl.setFont(new Font("Monospace", Font.PLAIN, 16));
	        lbl.setBounds(20,10,90,50);
	        jf.add(lbl);
	       final JComboBox<String> Nbox= new JComboBox<String>();
               Nbox.removeAllItems();
                Nbox.addItem("Select Usertype");
                Nbox.addItem("user");
                Nbox.addItem("admin");
	        Nbox.setBorder( BorderFactory.createEmptyBorder(8, 8, 2, 2));
	        Nbox.setLayout(null);
	        Nbox.setBounds(110,20,150,30);
	        jf.add(Nbox);  
	      
	        JLabel lbl1=new JLabel("UserName:"); 
	        lbl1.setForeground(Color.red);
	        lbl1.setLayout(null);
	        lbl1.setFont(new Font("Monospace", Font.PLAIN, 16));
	        lbl1.setBounds(20,55,90,50);
	        jf.add(lbl1);
	        final JTextField Ubox = new JTextField(); //USERNAME
	        Ubox.setBorder( BorderFactory.createEmptyBorder(8, 8, 2, 2));
	        Ubox.setLayout(null);
	        Ubox.setBounds(120,65,150,30);
	        jf.add(Ubox);  
	  
		     JLabel lbl4=new JLabel("Password"); 
		        lbl4.setForeground(Color.red);
		        lbl4.setLayout(null);
		        lbl4.setFont(new Font("Monospace", Font.PLAIN, 16));
		        lbl4.setBounds(20,100,90,50);
		        jf.add(lbl4);
                        
                        
		        final JPasswordField Pbox = new JPasswordField(); //Password
		        Pbox.setBorder( BorderFactory.createEmptyBorder(8, 8, 2, 2));
		        Pbox.setLayout(null);
		        Pbox.setBounds(120,110,150,30);
		        jf.add(Pbox); 
		        final JRadioButton show=new JRadioButton("Show password");
		        show.setBounds(20,150,120,40);
		        jf.add(show);
		        
		        JButton btn= new JButton("Signup");
		        btn.setLayout(null);
		        btn.setBounds(100,200,110,40);
	     	    jf.add(btn);
	     	  
		
		        
		  show.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent ae) {
				   

				   if (show.isSelected()) {
				      Pbox.setEchoChar((char)0); //password = JPasswordField
				   } else {
				      Pbox.setEchoChar('*');
				   }
				   
			   }
			   
			   
		   });
		   
		   
	btn.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent ae) {
			
		    String user=Nbox.getSelectedItem().toString();
			String username=Ubox.getText().toString();
			String passText = new String(Pbox.getPassword());
			
			 if(username.length()==0||passText.length()==0)
				 JOptionPane.showMessageDialog(null, "Fields cannot be empty.", "Error", JOptionPane.ERROR_MESSAGE);
			 else if (!TxtCompare.compare(username, "uname")) {
			    JOptionPane.showMessageDialog(null, "Please enter a valid character", "Error", JOptionPane.ERROR_MESSAGE);
                                        Ubox.requestFocus();
			    }  
				else if (!TxtCompare.compare(passText,"pwd")) { 
		              
				 JOptionPane.showMessageDialog(null, "Your password must"+"\n"
                                            + "Be between 8 and 40 characters long"+"\n" +
                                              "Contain at least one digit." +"\n" +
                                              "Contain at least one lower case character." +"\n" +
                                              "Contain at least one upper case character." +"\n" +
                                              "Contain at least on special character from [ @ # $ % ! . ].", "Error", JOptionPane.ERROR_MESSAGE);
                                    
                                 Pbox.requestFocus();
                                
                                }
                                else{
                                    passText = Encode.sha256(passText);
	   String sql ="insert into login(Username,Password,usertype) values('"+username+"','"+passText+"','"+user+"')";
                   append(sql,"Inserted");
			jf.dispose();
                        
                                }
			
		}
		
		
	});
        jf.setVisible(true);
	}
}
