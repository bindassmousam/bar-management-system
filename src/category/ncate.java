package category;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import login.TxtCompare;
public class ncate extends login.Connect{
	public ncate() {

	JFrame cate=new JFrame("New Category");
	cate.setSize(350,220);
	cate.setLocationRelativeTo(null);
	cate.setLayout(null);
		
	
			JLabel ncateg=new JLabel("Category Name:");
			ncateg.setBounds(10,20,100,20);
			cate.add(ncateg);
			
			
		final JTextField txt3=new JTextField();
		txt3.setBounds(110,20,140,20);
		cate.add(txt3);
                
                
               
		 JLabel nsubcate=new JLabel("Sub-Category:");
                 nsubcate.setBounds(10,60,100,20);
                 cate.add(nsubcate);
                 
                 
                 final JTextField txt4=new JTextField();
                 txt4.setBounds(110,60, 140,20);
                 cate.add(txt4);
		
				JButton btn33=new JButton("Add");
				btn33.setBounds(100,100,90,30);
				cate.add(btn33);
                                btn33.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				
			String name=txt3.getText().trim();
                        String subname=txt4.getText().trim();
                        if(name.length()==0||subname.length()==0)
				 JOptionPane.showMessageDialog(null, "Fields cannot be empty.", "Error", JOptionPane.ERROR_MESSAGE);
			 else if (!TxtCompare.compare(name, "AasN")) {
			    JOptionPane.showMessageDialog(null, "Please enter a valid character", "Error", JOptionPane.ERROR_MESSAGE);
			    }  
				else if (!TxtCompare.compare(subname,"AasN")) { 
		              
				 JOptionPane.showMessageDialog(null, "Please enter a valid character", "Error", JOptionPane.ERROR_MESSAGE);
			 }
				//String gen=bgroup.getSelection().;
				
			    else{
                        
                                try {
                                    int updatedId=0;
                                    String sq="SELECT * FROM category WHERE CName='"+name+"'";
                                    ResultSet rsq = display(sq);
                                    if(rsq.next()){
                                        updatedId=rsq.getInt(1);
                                    }
                                    else{
                                        String sql="INSERT INTO category (CName) VALUES ('"+name+"')";
                                        exec(sql,Statement.RETURN_GENERATED_KEYS);
                                        ResultSet r = s.getGeneratedKeys();
                                        r.next();
                                        updatedId = r.getInt(1);
                                    }
                                    if(updatedId!=0){
                                        String sqll="SELECT * FROM sub_category WHERE subName='"+subname+"'";
                                        ResultSet rsql = display(sqll);
                                    if(rsql.next())
                                        JOptionPane.showMessageDialog(null, "Sub Category already exists.");
                                    else{
                                        String sql2="INSERT INTO sub_category(Category_ID,subName) VALUES ('"+updatedId+"','"+subname+"')";
                                        append(sql2,"Inserted");
                                    }
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger(ncate.class.getName()).log(Level.SEVERE, null, ex);
                                }    
                                }

				
				
			
			}
			
		});
		cate.add(btn33);
		JButton clear=new JButton("Clear");
		clear.setBounds(210,100,100,30);
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				txt3.setText("");
                                txt4.setText("");
				
				
				
			}
		});
		cate.add(clear);
		
                                
                                
			
			cate.setVisible(true);
	
}
}

