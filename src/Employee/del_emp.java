/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employee;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import login.Connect;

public class del_emp extends Connect{

	public del_emp(final String ID) {
		final JFrame del=new JFrame("Delete Employee");
		del.setSize(350,150);
		del.setLayout(null);
		del.setLocationRelativeTo(null);
		
		JLabel ask=new JLabel("Do you want to delete Employee ID: " + ID );
		ask.setBounds(1,10,330,50);
		ask.setHorizontalAlignment(JLabel.CENTER);
		del.getContentPane().add(ask, BorderLayout.CENTER);
		
		JButton yes=new JButton("YES");
		yes.setBounds(60,70,100,30);
		del.add(yes);
		
		JButton no=new JButton("NO");
		no.setBounds(180,70,100,30);
		del.add(no);
		
		yes.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
					String sql="DELETE FROM `employee` WHERE `employee`.`Emp_Id` = '"+ID+"'";
					append(sql,"Deleted");
					del.dispose();
					
			}

		});
		
		no.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				del.dispose();
				
			}
			});
		
		
		
		
		del.setVisible(true);

}

}
