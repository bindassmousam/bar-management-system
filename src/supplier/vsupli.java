package supplier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class vsupli extends login.Connect {
	public vsupli() {
		JFrame vsupl = new JFrame("Supplier details");

		String column[] = { "SID", "Supplier Name", "Address", "Phone No", "Email", "Vat/Pan No." };
		final DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(column);

		final JTable jt = new JTable();
		jt.setModel(model);
		jt.getColumnModel().getColumn(0).setPreferredWidth(5);
		jt.getColumnModel().getColumn(1).setPreferredWidth(120);
		jt.getColumnModel().getColumn(2).setPreferredWidth(100);
		jt.getColumnModel().getColumn(3).setPreferredWidth(50);
		jt.getColumnModel().getColumn(4).setPreferredWidth(170);
		jt.getColumnModel().getColumn(5).setPreferredWidth(100);

		final JScrollPane sp = new JScrollPane(jt);
		sp.setBounds(40, 50, 800, 200);
		vsupl.add(sp);
		vsupl.setSize(900, 400);
		try{
			String sql="SELECT * FROM `supplier` ";
			ResultSet rs=display(sql);
			while(rs.next()){
				
				model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6)});
				
			}
		}catch(Exception E){
			
		}
		

		JLabel searchL = new JLabel("Search :");
		searchL.setBounds(10, 10, 50, 20);
		vsupl.add(searchL);

		final JTextField searchT = new JTextField();
		searchT.setBounds(60, 10, 300, 20);
		vsupl.add(searchT);
		searchT.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String sql;
				if (searchT.getText().length() != 0) {
					sql = "SELECT * FROM `supplier` WHERE `Sname` LIKE '" + searchT.getText().toString() + "%'";
				} else {
					sql = "SELECT * FROM `supplier`";
				}

				try {
					ResultSet rs = display(sql);
					sp.repaint();
					model.getDataVector().removeAllElements();
					while (rs.next()) {
						model.addRow(new Object[] { rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
								rs.getString(5),rs.getString(6) });
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		JButton abtn = new JButton("Add New");
		abtn.setBounds(440, 280, 100, 30);
		vsupl.add(abtn);
		abtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
                                vsupl.dispose();
				new nsupli();
			}
		});

		JButton ubtn = new JButton("Update");
		ubtn.setBounds(200, 280, 100, 30);
		vsupl.add(ubtn);
		ubtn.addActionListener(new ActionListener() {
			String ID;

			public void actionPerformed(ActionEvent ae) {

				int[] row = jt.getSelectedRows();
                                if(row.length<=0){
                                                                JOptionPane.showMessageDialog(null, "Please Select a supplier.");
                                                                return;
                                                            }
				for (int i = 0; i < row.length; i++) {
					ID = (String) jt.getValueAt(row[i], 0);

				}
                                vsupl.dispose();
				new up_supp(ID);
			}

		});

		JButton dbtn = new JButton("Delete");
		dbtn.setBounds(320, 280, 100, 30);
		vsupl.add(dbtn);
		dbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int[] row = jt.getSelectedRows();
                                if(row.length<=0){
                                                                JOptionPane.showMessageDialog(null, "Please Select a supplier.");
                                                                return;
                                                            }
				String ID = null;
				for (int i = 0; i < row.length; i++) {
					ID = (String) jt.getValueAt(row[i], 0);

				}
                                vsupl.dispose();
				new del_supp(ID);
			}
		});

		vsupl.setLocationRelativeTo(null);
		vsupl.setLayout(null);
		vsupl.setVisible(true);
	}
}
