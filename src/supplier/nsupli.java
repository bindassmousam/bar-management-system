package supplier;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import login.TxtCompare;
public class nsupli extends login.Connect {
			
	public nsupli() {
		


	JFrame nsu=new JFrame("Supplier details");
	nsu.setSize(350,320);
	nsu.setLocationRelativeTo(null);
	nsu.setLayout(null);
		
	
		JLabel user=new JLabel("Supplier Name:");
		user.setBounds(10,10,150,20);
		nsu.add(user);
		
		final JTextField use1=new JTextField();
		use1.setBounds(100,10,150,20);
		nsu.add(use1);
		
		
		JLabel user4=new JLabel("Address:");
		user4.setBounds(10,40,150,20);
		nsu.add(user4);
		
		final JTextField use4=new JTextField();
		use4.setBounds(100,40,150,20);
		nsu.add(use4);
		
		JLabel user5=new JLabel("Ph.No:");
		user5.setBounds(10,70,150,20);
		nsu.add(user5);
		
		final JTextField use5=new JTextField();
		use5.setBounds(100,70,150,20);
		nsu.add(use5);
	
		JLabel user7=new JLabel("Email:");
		user7.setBounds(10,100,150,20);
		nsu.add(user7);
		
		final JTextField use6=new JTextField();
		use6.setBounds(100,100,150,20);
		nsu.add(use6);
		
		JLabel user8=new JLabel("Vat/Pan No.");
		user8.setBounds(10,130,150,20);
		nsu.add(user8);
		
		final JTextField use8=new JTextField();
		use8.setBounds(100,130,150,20);
		nsu.add(use8);
		
		JButton btn2=new JButton("Insert");
		btn2.setBounds(70,170,100,40);
		nsu.add(btn2);
		btn2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				
                                String name=use1.getText();
				String add=use4.getText();
				String con=use5.getText();
				String mail=use6.getText();
				String vat=use8.getText();
				
				if(name.length()==0||add.length()==0||vat.length()==0||mail.length()==0)
				 JOptionPane.showMessageDialog(null, "Fields cannot be empty.", "Error", JOptionPane.ERROR_MESSAGE);
			 else if (!TxtCompare.compare(name, "Aas")) {
			    JOptionPane.showMessageDialog(null, "Please enter a valid character", "Error", JOptionPane.ERROR_MESSAGE);
			    
			    }  
				else if(!TxtCompare.compare(mail,"E")) {
				 JOptionPane.showMessageDialog(null, "Please enter a valid Email address", "Error", JOptionPane.ERROR_MESSAGE);

		     }
                                else if (!TxtCompare.compare(vat,"N")) { 
		              
				 JOptionPane.showMessageDialog(null, "Please enter a valid number", "Error", JOptionPane.ERROR_MESSAGE);
			 }
				
                                else{	
				
//				if(userin==null) {
				String sql="INSERT INTO supplier (Sname, Address, Contact,Email,Vat_Pan) VALUES ('"+name+"', '"+add+"', '"+con+"','"+mail+"','"+vat+"')";          
				
				append(sql,"Inserted");
                                nsu.dispose();
  		
				}
				
				
                        }	
			
		});
		JButton clear=new JButton("Clear");
		clear.setBounds(190,170,100,40);
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				use1.setText("");
				use4.setText("");
				use5.setText("");
				use6.setText("");
				use8.setText("");
				
			}
		});
		nsu.add(clear);
		
		nsu.setVisible(true);
	
}
}


