package product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import login.TxtCompare;


public class nprod extends login.Connect {
	public nprod(){
	JFrame jf=new JFrame("New Product");
	jf.setSize(400,300);
	jf.setLocationRelativeTo(null);
	jf.setLayout(null);
	
	JLabel user=new JLabel("Product Name:");
	user.setBounds(10,10,150,20);
	jf.add(user);
	
	final JTextField use1=new JTextField();
	use1.setBounds(100,10,245,20);
	jf.add(use1);
	
        JLabel users=new JLabel("Quantity:");
	users.setBounds(10,35,100,20);
	jf.add(users);
	final JComboBox<String> cmb3 = new JComboBox<String>();
        
        cmb3.setBounds(100,35,100,20);
        jf.add(cmb3);
        cmb3.addItem("125 ml");
        cmb3.addItem("250 ml");
        cmb3.addItem("500 ml");
        cmb3.addItem("1000 ml");
        JLabel rate=new JLabel("Sales rate:");
	rate.setBounds(10,65,150,20);
	jf.add(rate);
	
	final JTextField use2=new JTextField();
	use2.setBounds(100,65,245,20);
	jf.add(use2);
	
	JLabel user6=new JLabel("Category:");
	user6.setBounds(10,100,100,20);
	jf.add(user6);
	final JComboBox<String> cmb1 = new JComboBox<String>();
		cmb1.addItem("Select Category");
		try{
			String sql="SELECT `Category_Id`, `Cname` FROM `category` ORDER BY Cname ";
			ResultSet rs=display(sql);
			while(rs.next()){
				cmb1.addItem(rs.getString(1)+" | "+rs.getString(2)); 
			}
		}catch(Exception e){
			
		}
	final JComboBox<String> cmb2 = new JComboBox<String>();        
        cmb2.addItem("Select Sub Category");
	cmb1.setBounds(100,100, 245, 30);
        cmb1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                cmb2.removeAllItems();
                String cate=cmb1.getSelectedItem().toString();
                if(!cate.equals("Select Category")){
                    String id1;
                    int temp=cate.indexOf(" | ");
                    id1=cate.substring(0,temp);
                    ResultSet rs=display("Select * From sub_category where Category_ID="+id1+" Order By subName");
                    try {
                        while(rs.next()){
                            cmb2.addItem(rs.getString("subName"));
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(nprod.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
		
		jf.add(cmb1);
	
                JLabel user7=new JLabel("Sub Category:");
	user7.setBounds(10,140,100,20);
	jf.add(user7);
	
	cmb2.setBounds(100,140, 245, 30);
		
		jf.add(cmb2);
	
                
                
                
	
	
	JButton btn2=new JButton("Insert");
	btn2.setBounds(90,200,100,40);
	btn2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                String combo=cmb1.getSelectedItem().toString();
                String comb=cmb2.getSelectedItem().toString();
                String qun=cmb3.getSelectedItem().toString();
                if(use1.getText().length()==0)
                    JOptionPane.showMessageDialog(null, "fields ccannot be empty.", "Error", JOptionPane.ERROR_MESSAGE);
                else if (!TxtCompare.compare(use1.getText(), "AasN"))
                    JOptionPane.showMessageDialog(null, "Please enter a valid name", "Error", JOptionPane.ERROR_MESSAGE); 
                else if(use2.getText().length()==0)
                    JOptionPane.showMessageDialog(null, "Rate ccannot be empty.", "Error", JOptionPane.ERROR_MESSAGE);
                else if (!TxtCompare.compare(use2.getText(), "N"))
                    JOptionPane.showMessageDialog(null, "Please enter a valid rate", "Error", JOptionPane.ERROR_MESSAGE); 
                else if(combo.equals("Select Category"))
                    JOptionPane.showMessageDialog(null, "Please select a valid category", "Error", JOptionPane.ERROR_MESSAGE); 
                else{
                    String name=use1.getText();
                    String srate = use2.getText();
                    name = name+" ("+qun+")";
                    combo = combo.substring(0,combo.indexOf(" | "));
                    try{
                        s.execute("CREATE TABLE IF NOT EXISTS product("
                                + "Product_Id INT PRIMARY KEY AUTO_INCREMENT,"
                                + "Product_name VARCHAR(50) NOT NULL,"
                                + "Category VARCHAR(5) NOT NULL,"
                                + "Sub_Category VARCHAR(50) NOT NULL,"
                                + "sales_rate varchar(30) NOT NULL"
                                + ")"
                        );
                        String sqll="SELECT * FROM product WHERE Category='"+combo+"' AND Sub_Category='"+comb+"' AND Product_name='"+name+"'";
                        ResultSet rsql = display(sqll);
                        if(rsql.next())
                            JOptionPane.showMessageDialog(null, "Product already exists.");
                        else{
                            String sql="INSERT INTO product(Product_name,Category,Sub_Category,sales_rate) VALUES('"+name+"', '"+combo+"', '"+comb+"', '"+srate+"')";
                            s.execute(sql);
                            JOptionPane.showMessageDialog(null, "Product inserted.");
                    
                            use1.setText("");
                            use2.setText("");
                            cmb1.setSelectedIndex(0);
                            cmb3.setSelectedIndex(0);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
	});
        jf.add(btn2);
	jf.setVisible(true);
	
	
	
	}
	
        @Override
	public void append(String sql, String string) {
		// TODO Auto-generated method stub
		
	}
}