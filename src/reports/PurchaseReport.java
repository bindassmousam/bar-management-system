/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;


import common_modules.Date_Chooser;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import login.Connect;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


public class PurchaseReport {
    JTextField btn3=new JTextField();
    JTextField btn=new JTextField();
    JFrame jframe= new JFrame("Purchase Report");
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public PurchaseReport(){
        jframe.setSize(300,170);
        jframe.setLocationRelativeTo(null);
        jframe.setLayout(null);
        jframe.setResizable(false);

        JLabel lbl6=new JLabel("From:");
        lbl6.setBounds(10,15,40,20);
        jframe.add(lbl6);

        JLabel lbl9 = new JLabel("To:");
        lbl9.setBounds(10,50,40,20);
        jframe.add(lbl9);

        btn3.setEnabled(false);
        btn3.setBounds(60,15,160,30);
        jframe.add(btn3);

        btn.setEnabled(false);
        btn.setBounds(60,50,160,30);
        jframe.add(btn);
        jframe.setVisible(true);

        JButton b= new JButton("...");
        b.addActionListener((ActionEvent e) -> {
            new Date_Chooser(btn3);
        });
        b.setBounds(240,15,30,30);
        jframe.add(b);

        JButton b1= new JButton("...");
        b1.addActionListener((ActionEvent e) -> {
            new Date_Chooser(btn);
        });
        b1.setBounds(240,50,30,30);
        jframe.add(b1);

        JButton b11= new JButton("Genrate");
        b11.addActionListener((ActionEvent e) -> {
            generate();
        });
        b11.setBounds(100,100,100,30);
        jframe.add(b11);
    }
    @SuppressWarnings("UseSpecificCatch")
    private void generate() {
        if(btn3.getText().length()<=0 || btn.getText().length()<=0){
            JOptionPane.showMessageDialog(null, "Input Date First.");
            return;
        }
        Connect conn = new Connect();
        conn.getConnect();
        try {
            HashMap param = new HashMap();
            String jrxmlLoc = getClass().getResource("/reports/PurchaseReport.jrxml").toString();
            jrxmlLoc = jrxmlLoc.substring(jrxmlLoc.indexOf(":")+1);
            JasperReport jr = JasperCompileManager.compileReport(JRXmlLoader.load(new FileInputStream(new File(jrxmlLoc))));
            param.put("date1", btn3.getText()+" 0:0:0");
            param.put("date2", btn.getText()+" 23:59:59");
            @SuppressWarnings("static-access")
            ResultSet rs = conn.s.executeQuery("SELECT SUM(rate*quantity) FROM purchase WHERE purchase_date BETWEEN '"+btn3.getText()+" 0:0:0' AND '"+btn.getText()+" 23:59:59'");
            if(rs.next()){
                if(rs.getDouble(1)>0){
                    param.put("total", rs.getDouble(1)+"");
                    @SuppressWarnings("static-access")
                    JasperPrint j = JasperFillManager.fillReport(jr,param,conn.conn);
                    JasperViewer.viewReport(j,false);
                    jframe.dispose();
                }
                else{
                    JOptionPane.showMessageDialog(null, "No Record Found.");
                }
            }
            else{
                JOptionPane.showMessageDialog(null, "No Record Found.");
            }
        } catch (Exception ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, ex);
        }
    }
}