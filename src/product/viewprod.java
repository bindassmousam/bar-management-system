package product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;


public class viewprod extends login.Connect {
public	viewprod(){
	JFrame pf=new JFrame();
	 pf.setTitle("Product");
	 
	 
	 String[] columnNames = { "Product Id","Product Name" };
     final DefaultTableModel model=new DefaultTableModel();
		final JTable view = new JTable();
		
		//table	
		
			
		view.setModel(model);
		model.setColumnIdentifiers(columnNames);
		 
	
	view.setRowHeight(25);
	view.getColumnModel().getColumn(0).setPreferredWidth(10);
        view.getColumnModel().getColumn(1).setPreferredWidth(200);
	view.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	final JScrollPane sp=new JScrollPane(view);
	sp.setBounds(20,45,400,360);
	 
	 
	 try {
			String sql="SELECT * FROM `product`";
			ResultSet rs=display(sql);
			
			while(rs.next()){
				model.addRow(new String[]{rs.getString(1),rs.getString(2)});
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	 

		JLabel search=new JLabel("Search by Product name:");
		search.setBounds(20,5,150,30);
		pf.add(search);
		
		final JTextField Search=new JTextField();
		Search.setBounds(185,5,145,30);
		pf.add(Search);
		Search.addKeyListener(new KeyAdapter() {
	        public void keyReleased(KeyEvent e) {
	        	try {
	        	String sql="SELECT * FROM `product` WHERE `Product_name` LIKE '%"+Search.getText().toString()+"%'";
	            ResultSet rs=display(sql);
	            sp.repaint();
	        	model.getDataVector().removeAllElements();
	        	while(rs.next()){
	            	model.addRow(new Object[]{rs.getString(1).toString(),rs.getString(2).toString()});
					}
	        	} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
         
	        	}
	        });
		
		
		JButton Add= new JButton("Add New");
		Add.setBounds(40,410,145,30);
		pf.add(Add);
		
		Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
             
			try {
                            pf.dispose();
				new nprod();
				
			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			}
		});
	
		
		JButton Update= new JButton("Update");
		Update.setBounds(230,410,145,30);
		pf.add(Update);
		Update.addActionListener(new ActionListener() {
                    String ID;
			public void actionPerformed(ActionEvent ae) {
				   
				    int row = view.getSelectedRow();
                                     if(row<0){
                                                                JOptionPane.showMessageDialog(null, "Please Select a product.");
                                                                return;
                                                            }
	                    ID = (String) view.getValueAt(row, 0);  
	                     
				
				
		pf.dispose();
 		new up_prod(ID);	
			}
		});
				
		pf.setSize(450,500);
		pf.setLocationRelativeTo(null);
		pf.setLayout(null);
	
		pf.add(sp);
                pf.setVisible(true);
	}
    }
