package category;



import java.awt.BorderLayout;
import java.awt.event.*;

import javax.swing.*;

public class del_cate extends login.Connect {

	
	del_cate(final String id){
		
		
		final JFrame del=new JFrame("Delete Category");
		del.setSize(350,150);
		del.setLayout(null);
		del.setLocationRelativeTo(null);
		
		JLabel ask=new JLabel("Do you want to delete category ID: " + id );
		ask.setBounds(1,10,330,50);
		ask.setHorizontalAlignment(JLabel.CENTER);
		del.getContentPane().add(ask, BorderLayout.CENTER);
		
		JButton yes=new JButton("YES");
		yes.setBounds(60,70,100,30);
		del.add(yes);
		yes.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent ae) {
				
                                String sql1="DELETE FROM `sub_category` WHERE Category_ID = '"+id+"'";
                                String sql="DELETE FROM `category` WHERE Category_ID = '"+id+"'";
				append(sql1,"Deleted");
                                append(sql,"Deleted");
				del.dispose();
				new disp_cate();
				
			}
		});
		
		JButton no=new JButton("NO");
		no.setBounds(180,70,100,30);
		del.add(no);
		no.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				del.dispose();
				
				
			}
		});
		
		
		
		del.setVisible(true);
		}
}
