package customer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class cview extends login.Connect{
    ctable jt;
    JScrollPane sp;
public cview() {
	JFrame cview=new JFrame("Customer details");
	
					  
					
			 		
			 		
			 		
			 		String column[]={"CID","Name","Address","Phone No","Gender","Email"};         
                                        final DefaultTableModel model=new DefaultTableModel(){
                                            public boolean isCellEditable(int row, int column){
                                                return false;
                                            }
                                        };
                                        model.setColumnIdentifiers(column);
                                        String sss="SELECT * FROM `customer` ";
                                        ResultSet rrr = display(sss);
			 		jt = new ctable(rrr,model);
			 		
					sp=new JScrollPane(jt);    
			 		sp.setBounds(40,50,770,200);
			 		cview.add(sp);          
			 		cview.setSize(850,400);
			 		
			 		final JLabel searchL=new JLabel("Search :");
					searchL.setBounds(10,10,50,20);
					cview.add(searchL);
					
					
					
					final JTextField searchT=new JTextField();
					searchT.setBounds(60,10,300,20);
					cview.add(searchT);
					searchT.addKeyListener(new KeyAdapter() {
                                            public void keyReleased(KeyEvent e) {	
                                                String sql="SELECT * FROM `customer` WHERE `CName` LIKE '%"+searchT.getText().toString()+"%' || `Customer_ID` LIKE '%"+searchT.getText().toString()+"%'";
                                                ResultSet rs=display(sql);
    				        	model.getDataVector().removeAllElements();
                                                jt = new ctable(rs,model);
                                                jt.repaint();
                                               // jt.revalidate();
                                                sp.repaint();
                                               // sp.revalidate();
                                            }
				        });
					
					JButton abtn=new JButton("Add New");
					abtn.setBounds(440,280,100,30);
					cview.add(abtn);
					abtn.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent ae){
                                                                cview.dispose();
								new ncustomer();					
							}
					});				
					
					JButton clear=new JButton("CLEAR");
					clear.setBounds(140,170,80,30);
					cview.add(clear);
					
					JButton ubtn= new JButton("Update");
					ubtn.setBounds(200,280,100,30);
					cview.add(ubtn);
					ubtn.addActionListener(new ActionListener(){
						String ID;
						public void actionPerformed(ActionEvent ae) {
							
							
							      
							    int[] row = jt.getSelectedRows();    
                                                            if(row.length<=0){
                                                                JOptionPane.showMessageDialog(null, "Please Select a customer.");
                                                                return;
                                                            }
							    for (int i = 0; i < row.length; i++) {   
				                    ID = (String) jt.getValueAt(row[i],0);  
				                    
				                } 
							cview.dispose();
							new Cus_up(ID);
				                }
                                             
						
					});
					 
					
					JButton dbtn=new JButton("Delete");
					dbtn.setBounds(320,280,100,30);
					cview.add(dbtn);
					dbtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent ae) {
							
							    int[] row = jt.getSelectedRows();
                                                            if(row.length<=0){
                                                                JOptionPane.showMessageDialog(null, "Please Select a customer.");
                                                                return;
                                                            }
							    String ID=null;
							    for (int i = 0; i < row.length; i++) {   
				                    ID = (String) jt.getValueAt(row[i],0);  
				                        
				                } 
                                                            cview.dispose();
			    		new Cus_del(ID);	
						
							    }
					});
					   jt.repaint();
					cview.setLocationRelativeTo(null);
					cview.setLayout(null);
			 		cview.setVisible(true);   			    
}
}

