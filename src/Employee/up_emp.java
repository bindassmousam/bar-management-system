/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employee;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;



public class up_emp extends login.Connect{

	public up_emp(final String id) {
		getConnect();
			
		
		JFrame cup=new JFrame("Employee details");
		cup.setSize(400,300);
		cup.setLocationRelativeTo(null);
		cup.setLayout(null);
			
		
			JLabel user=new JLabel("Name:");
			user.setBounds(10,10,150,20);
			cup.add(user);
			
			final JTextField use1=new JTextField();
			use1.setBounds(90,10,150,20);
			cup.add(use1);
			
			JLabel user4=new JLabel("Address:");
			user4.setBounds(10,40,150,20);
			cup.add(user4);
			
			final JTextField use4=new JTextField();
			use4.setBounds(90,40,150,20);
			cup.add(use4);
			
			JLabel user5=new JLabel("Ph.No:");
			user5.setBounds(10,70,150,20);
			cup.add(user5);
			
			final JTextField use5=new JTextField();
			use5.setBounds(90,70,150,20);
			cup.add(use5);
			
			
			JLabel user6=new JLabel("Gender:");
			user6.setBounds(10,100,100,20);
			cup.add(user6);
			
			
			JRadioButton maleButton=new JRadioButton("Male");
			JRadioButton femaleButton=new JRadioButton("Female");
			JRadioButton otherButton=new JRadioButton("Other");
			final ButtonGroup bgroup = new ButtonGroup();
			bgroup.add(maleButton);
			bgroup.add(femaleButton);
			bgroup.add(otherButton);
			cup.add(maleButton);
			maleButton.setBounds(90,100,60,20);
			maleButton.setActionCommand("Male");
			cup.add(femaleButton);
			femaleButton.setBounds(150,100,70,20);
			femaleButton.setActionCommand("Female");
			cup.add(otherButton);
			otherButton.setBounds(220,100,80,20);
			
			
			JLabel user7=new JLabel("Post:");
			user7.setBounds(10,130,150,20);
			cup.add(user7);
			
			final JTextField use6=new JTextField();
			use6.setBounds(90,130,200,20);
			cup.add(use6);
		
		JButton update=new JButton("Update");
		update.setBounds(40,170,80,30);
		cup.add(update);
		

		JButton clear=new JButton("CLEAR");
		clear.setBounds(140,170,80,30);
		cup.add(clear);
	
		try{
			
			String sql="SELECT * FROM `employee` WHERE `employee`.`Emp_Id`='"+id+"'";
			ResultSet rs=display(sql);
			while(rs.next()){
				use1.setText(rs.getString(2));
				use4.setText(rs.getString(3));
				use5.setText(rs.getString(5));
				String gender=rs.getString(4); 
				if (gender.equals("Male")) {
				        maleButton.setSelected(true); 
				    } else if(gender.equals("Female")){
				        femaleButton.setSelected(true); 
				    } else {
				    	otherButton.setSelected(true);
				    }
				    
				
				use6.setText(rs.getString(6));
				}
				}
			catch(Exception e) {
				
			}
		
		
		
		update.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				
				String name=use1.getText();
				String add=use4.getText();
				String con=use5.getText();
				String gen = bgroup.getSelection().getActionCommand();
				String post=use6.getText();
		
		String sql2="UPDATE `employee` SET `Name`='"+name+"',`Address`='"+add+"',`Gender`='"+gen+"',"
		    	+ "`Contact_No`='"+con+"',`Position`='"+post+"' WHERE `Emp_Id`= '"+id+"'";          
				append(sql2,"Updated");
				
				
				cup.dispose();
				
			}
		});
                
		
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				use1.setText("");
				use4.setText("");
				use5.setText("");
				use6.setText("");
				
			}
		});
			
		cup.setVisible(true);
		
	}
}