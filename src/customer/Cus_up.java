package customer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

import javax.swing.*;

public class Cus_up extends login.Connect{

	public Cus_up(final String id) {
		getConnect();
			
		
		JFrame cup=new JFrame("Customer details");
		cup.setSize(400,300);
		cup.setLocationRelativeTo(null);
		cup.setLayout(null);
			
		
			JLabel user=new JLabel("Name:");
			user.setBounds(10,10,150,20);
			cup.add(user);
			
			final JTextField use1=new JTextField();
			use1.setBounds(90,10,150,20);
			cup.add(use1);
			
			JLabel user4=new JLabel("Address:");
			user4.setBounds(10,40,150,20);
			cup.add(user4);
			
			final JTextField use4=new JTextField();
			use4.setBounds(90,40,150,20);
			cup.add(use4);
			
			JLabel user5=new JLabel("Ph.No:");
			user5.setBounds(10,70,150,20);
			cup.add(user5);
			
			final JTextField use5=new JTextField();
			use5.setBounds(90,70,150,20);
			cup.add(use5);
			
			
			JLabel user6=new JLabel("Gender:");
			user6.setBounds(10,100,100,20);
			cup.add(user6);
			
			
			JRadioButton maleButton=new JRadioButton("Male");
			JRadioButton femaleButton=new JRadioButton("Female");
			JRadioButton otherButton=new JRadioButton("Other");
			final ButtonGroup bgroup = new ButtonGroup();
			bgroup.add(maleButton);
			bgroup.add(femaleButton);
			bgroup.add(otherButton);
			cup.add(maleButton);
			maleButton.setBounds(90,100,60,20);
			maleButton.setActionCommand("Male");
			cup.add(femaleButton);
			femaleButton.setBounds(150,100,70,20);
			femaleButton.setActionCommand("Female");
			cup.add(otherButton);
			otherButton.setBounds(220,100,80,20);
			
			
			JLabel user7=new JLabel("Email:");
			user7.setBounds(10,130,150,20);
			cup.add(user7);
			
			final JTextField use6=new JTextField();
			use6.setBounds(90,130,200,20);
			cup.add(use6);
		
		JButton update=new JButton("Update");
		update.setBounds(40,170,80,30);
		cup.add(update);
		

		JButton clear=new JButton("CLEAR");
		clear.setBounds(140,170,80,30);
		cup.add(clear);
	
		try{
			
			String sql="SELECT * FROM `customer` WHERE `customer`.`Customer_ID`='"+id+"'";
			ResultSet rs=display(sql);
			while(rs.next()){
				use1.setText(rs.getString(2));
				use4.setText(rs.getString(3));
				use5.setText(rs.getString(4));
				String gender=rs.getString(5); 
				if (gender.equals("Male")) {
				        maleButton.setSelected(true); 
				    } else if(gender.equals("Female")){
				        femaleButton.setSelected(true); 
				    } else {
				    	otherButton.setSelected(true);
				    }
				    
				
				use6.setText(rs.getString(6));
				}
				}
			catch(Exception e) {
				
			}
		
		
		
		update.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				
				String name=use1.getText();
				String add=use4.getText();
				String con=use5.getText();
				String gen = bgroup.getSelection().getActionCommand();
				String mail=use6.getText();
		
		String sql2="UPDATE `customer` SET `CName`='"+name+"',`Address`='"+add+"',`Contact_No`='"+con+"',"
		    	+ "`Gender`='"+gen+"',`Email`='"+mail+"' WHERE `Customer_ID`= '"+id+"'";          
				append(sql2,"Updated");
				
				
				cup.dispose();
				
			}
		});
                
		
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				use1.setText("");
				use4.setText("");
				use5.setText("");
				use6.setText("");
				
			}
		});
			
					cup.setVisible(true);
		
	}
//public static void main(String[] args) {
//	new Cus_up(null);
//}
}