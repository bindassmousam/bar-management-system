package login;

import Employee.eview;
import Employee.new_employee;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import category.ncate;
import product.nprod;
import product.viewprod;

import purchase.npurch;
import reports.reports;
import sales.sview;
import stock.new_stock;
import supplier.nsupli;
import supplier.vsupli;

public class Dashboard {
	String usertype = "";

        @SuppressWarnings("ResultOfObjectAllocationIgnored")
	Dashboard(String userType) {
		usertype = userType;
		JFrame jframe = new JFrame("BAR:" + userType);
		jframe.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		jframe.setLayout(null);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		jframe.setSize(screenSize.width, screenSize.height);
		jframe.setLocationRelativeTo(null);

		JMenuBar menu = new JMenuBar();
		menu.setBounds(0, 2, jframe.getWidth(), 30);
		jframe.add(menu);

		Font font1 = new Font("SansSerif", Font.BOLD, 20);
		JMenu cos = new JMenu("Customer");
		JMenuItem ncos = new JMenuItem("New Customer");
		JMenuItem vcos = new JMenuItem("View Customer");
		ncos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new customer.ncustomer();

			}
		});
		cos.add(ncos);

		  	JLabel jp=new JLabel();
	        jp.setBounds(70,100,jframe.getWidth()-200,700);
	        jp.setHorizontalAlignment(SwingConstants.CENTER);
			jp.setVerticalAlignment(SwingConstants.CENTER);
		
		JLabel Customertxt=new JLabel("Customer");
		Customertxt.setBounds(125,70,100,30);
		Customertxt.setFont(new Font("Serif", Font.PLAIN, 20));
		jp.add(Customertxt);
		
		JButton customer=new JButton();
		customer.setBounds(100, 100, 120, 100);
		Image custom=new ImageIcon(this.getClass().getResource("/customer/customer.png")).getImage();
		custom.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
		customer.setIcon(new ImageIcon(custom));
		jp.add(customer);
		customer.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent ae){
        		new customer.cview();
        		
        	}
        }); 
		
		
		
	//product 
		JLabel producttxt=new JLabel("Product");
		producttxt.setBounds(390,70,100,30);
		producttxt.setFont(new Font("Serif", Font.PLAIN, 20));
		jp.add(producttxt);
		
		JButton product=new JButton();
		product.setBounds(360, 100, 120, 100);
		Image productp=new ImageIcon(this.getClass().getResource("/product/drinks.png")).getImage();
		productp.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
		product.setIcon(new ImageIcon(productp));
		jp.add(product);
//		
		product.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent ae){
        		new viewprod();
        		
        	}
        });
                
                JLabel suppliertxt=new JLabel("Supplier");
		suppliertxt.setBounds(260,70,100,30);
		suppliertxt.setFont(new Font("Serif", Font.PLAIN, 20));
		
		
		JButton supplier=new JButton();
		supplier.setBounds(230, 100, 120, 100);
		Image supplierp=new ImageIcon(this.getClass().getResource("/supplier/supplier.png")).getImage();
		supplierp.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
		supplier.setIcon(new ImageIcon(supplierp));
		 if (userType.equals("admin")) {
                
                jp.add(suppliertxt);
                jp.add(supplier);
                 }
		supplier.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent ae){
        		new supplier.vsupli();
        		
        	}
        }); 
                
		//Sales 
		
//		
		//Purchase 
		JLabel Purchasetxt=new JLabel("Purchase");
		Purchasetxt.setBounds(530,70,100,30);
		Purchasetxt.setFont(new Font("Serif", Font.PLAIN, 20));
		
		
		JButton Purchase=new JButton();
		Purchase.setBounds(490, 100, 120, 100);
		Image Purchasep=new ImageIcon(this.getClass().getResource("/purchase/purchase.png")).getImage();
		Purchasep.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
		Purchase.setIcon(new ImageIcon(Purchasep));
		 if (userType.equals("admin")) {
                jp.add(Purchasetxt);
                jp.add(Purchase);
                 }
		Purchase.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent ae){
        		new purchase.npurch();
        		
        	}
        }); 
                
                JLabel Salestxt=new JLabel("Sales");
		Salestxt.setBounds(650,70,100,30);
		Salestxt.setFont(new Font("Serif", Font.PLAIN, 20));
		jp.add(Salestxt);
		
		JButton Sales=new JButton();
		Sales.setBounds(620, 100, 120, 100);
		Image Salesp=new ImageIcon(this.getClass().getResource("/sales/sales.png")).getImage();
		Salesp.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
		Sales.setIcon(new ImageIcon(Salesp));
		jp.add(Sales);
		Sales.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent ae){
        		new sales.sview();
        		
        	}
        }); 
//		
//		
		//Stocks 
		JLabel Stockstxt=new JLabel("Stocks");
		Stockstxt.setBounds(880,70,100,30);
		Stockstxt.setFont(new Font("Serif", Font.PLAIN, 20));
                jp.add(Stockstxt);
		
		
		JButton Stocks=new JButton();
		Stocks.setBounds(880, 100, 120, 100);
		Image Stocksp=new ImageIcon(this.getClass().getResource("/stock/stock.png")).getImage();
		Stocksp.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
		Stocks.setIcon(new ImageIcon(Stocksp));
                Stocks.addActionListener((ActionEvent e) -> {
                        new new_stock();
                });
                jp.add(Stocks);
		
              
		//Help	 
//		JLabel Helptxt=new JLabel("Help");
//		Helptxt.setBounds(1010,70,100,30);
//		Helptxt.setFont(new Font("Serif", Font.PLAIN, 20));
//        	jp.add(Helptxt);
//		
//		JButton Help=new JButton();
//                Help.setBounds(1010, 100, 120, 100);
//		Image Helpp=new ImageIcon(this.getClass().getResource("Help.png")).getImage();
//		Helpp.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
//		Help.setIcon(new ImageIcon(Helpp));
//		jp.add(Help);
				
//		
//		//Table
//		
//		
//		
		//REPORT 
		JLabel REPORT=new JLabel("Report");
                REPORT.setFont(new Font("Serif", Font.PLAIN, 20));
                REPORT.setBounds(780,70,100,30);
		
                
                JButton reportt=new JButton();
                reportt.setBounds(750, 100, 120, 100);
		Image report=new ImageIcon(this.getClass().getResource("report.png")).getImage();
		report.getScaledInstance(120, 100, Image.SCALE_SMOOTH);
		reportt.setIcon(new ImageIcon(report));
		 if (userType.equals("admin")) {
                     reportt.addActionListener((ActionEvent e) -> {
                         new reports();
                     });
                jp.add(REPORT);
                 jp.add(reportt);
                 }


//
		JLabel proj = new JLabel("BAR MANAGEMENT SYSTEM");
		proj.setFont(new Font("TimesNewRoman", Font.BOLD, 50));
		proj.setForeground(Color.RED);
		proj.setBounds(300, 0, 900, 50);
		jp.add(proj);

		JLabel copy = new JLabel("Designed and Developed By @Mousam Kharel and @Suman Adhikari");
		copy.setFont(new Font("SansSerif", Font.BOLD, 20));
		copy.setForeground(Color.RED);
		copy.setBounds(510, 540, 900, 30);
		jp.add(copy);
		 
		 
		 
		vcos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new customer.cview();

			}
		});
		cos.add(vcos);
		cos.setFont(font1);
		menu.add(cos);

		
		JMenu supp = new JMenu("Supplier");
		JMenuItem nsup = new JMenuItem("New Supplier");
		JMenuItem vsup = new JMenuItem("View Supplier");
		nsup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new nsupli();

			}
		});
		supp.add(nsup);
		vsup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new vsupli();

			}
		});
		supp.add(vsup);
		supp.setFont(font1);
		if (userType.equals("admin")) {
			menu.add(supp);
		}

		
		JMenu itemc = new JMenu("Item Category");
		JMenuItem ncat = new JMenuItem("New Category");
		ncat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new ncate();

			}
		});
		itemc.add(ncat);
		itemc.setFont(font1);
		menu.add(itemc);

		
		JMenu prod = new JMenu("Product");
		JMenuItem npro = new JMenuItem("New Product");
		JMenuItem vpro = new JMenuItem("View Product");
		npro.addActionListener(new ActionListener() {
                      
			public void actionPerformed(ActionEvent e) {

				new nprod();

			}
		});
                prod.add(npro);
		prod.add(vpro);
                vpro.addActionListener(new ActionListener() {
                      
			public void actionPerformed(ActionEvent e) {

				new viewprod();

			}
		});
                
		prod.setFont(font1);
		menu.add(prod);

		JMenu pur = new JMenu("Purchase");
		JMenuItem npur = new JMenuItem("New Purchase");
		npur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new npurch();

			}
		});
		pur.add(npur);
		pur.setFont(font1);

		if (userType.equals("admin")) {
			menu.add(pur);
		}

		JMenu sale = new JMenu("Sales");
		JMenuItem nsale = new JMenuItem("New Sales");
		nsale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new sview();

			}
		});

		sale.add(nsale);
		sale.setFont(font1);
		menu.add(sale);

		JMenu sto = new JMenu("Stock");
		JMenuItem nsto = new JMenuItem("Total Stock");
                
                
		sto.add(nsto);
                nsto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new new_stock();

			}
		});
		sto.setFont(font1);
		menu.add(sto);

		JMenu rpt = new JMenu("Report");
		JMenuItem drpt = new JMenuItem("View Reports");
		rpt.add(drpt);
               drpt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new reports();

			}
		});
		rpt.setFont(font1);
		if (userType.equals("admin")) {
			menu.add(rpt);
		}		
		
                JMenu  register=new JMenu("User Management");
                JMenuItem add=new JMenuItem("Add User");
                
                        add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new Register();

			}
		});
                         
                register.add(add);
                register.setFont(font1);
                if (userType.equals("admin")) {
                        menu.add(register);
                }
                
                
                JMenu emp=new JMenu("Employee");
                JMenuItem nemp=new JMenuItem("New Employee");
                  nemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new new_employee();

			}
		});
                
                
                JMenuItem uemp=new JMenuItem("View Employee");
                 uemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				new eview();

			}
		});
                
                
                emp.add(nemp);
                emp.add(uemp);
                emp.setFont(font1);
                if(userType.equals("admin")){
                menu.add(emp);
                }
                
                JMenu help = new JMenu("Help");
		JMenuItem uman = new JMenuItem("Help Contents");
		JMenuItem cdep = new JMenuItem("Contact Developer");
		JMenuItem abts = new JMenuItem("About Software");
		help.add(uman);
		help.add(abts);
		help.add(cdep);
		help.setFont(font1);
		menu.add(help);
                
                
		jframe.add(jp);

		jframe.setVisible(true);

        }
        }

class jpanel {
	jpanel() {

	}
}