package login;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 *
 * @author Gaurav Nyaupane "NIGHTMARE"
 * @project Third_Eye
 * @package common_modules
 * @file Encode.java
 * 
 * @created Aug 6, 2019 4:12:11 PM
 * --+ COPYRIGHT PROTECTED MATERIAL +--
 *
 **/
public class Encode {
    @SuppressWarnings("CallToPrintStackTrace")
    public static String sha256(String text){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashbyte = md.digest(text.getBytes(StandardCharsets.UTF_8));
            StringBuilder hashstring = new StringBuilder();
            for(int i=0;i<hashbyte.length;i++){
                String hex = Integer.toHexString(0xff & hashbyte[i]);
                if(hex.length() == 1)
                    hashstring.append('0');
                hashstring.append(hex);
            }
            String hash = hashstring.toString();
            return hash;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}