package login;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Login extends Connect {
	JFrame jframe = new JFrame("Login");
	JTextField txt = new JTextField();
	JPasswordField txt1 = new JPasswordField();

	Login() {
		getConnect();
		jframe.setSize(400, 230);
		jframe.setLocationRelativeTo(null);
		jframe.setLayout(null);

		Font font1 = new Font("SansSerif", Font.BOLD, 15);
		JLabel lbl = new JLabel("Username:");
		lbl.setForeground(Color.RED);
		lbl.setBounds(100, 20, 100, 20);
		lbl.setFont(font1);
		jframe.add(lbl);

		txt.setBounds(178, 20, 200, 20);
		jframe.add(txt);

		Font font2 = new Font("SansSerif", Font.BOLD, 15);
		JLabel lbl2 = new JLabel("Password:");
		lbl2.setForeground(Color.RED);
		lbl2.setBounds(100, 60, 100, 20);
		lbl2.setFont(font2);
		jframe.add(lbl2);

		txt1.setBounds(178, 60, 200, 20);
		txt1.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_ENTER) {
					loginnow();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {

			}

			@Override
			public void keyTyped(KeyEvent arg0) {

			}
		});
		jframe.add(txt1);
                
                 final JRadioButton show=new JRadioButton("Show password");
		        show.setBounds(100,90,120,40);
		        jframe.add(show);
                    show.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent ae) {
				   

				   if (show.isSelected()) {
				      txt1.setEchoChar((char)0); //password = JPasswordField
				   } else {
				      txt1.setEchoChar('*');
				   }
				   
			   }
			   
			   
		   });
		   
		JButton btn = new JButton("Login");
		btn.setForeground(Color.BLUE);
		btn.setBounds(180, 140, 90, 30);
		jframe.add(btn);

		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loginnow();

			}

		});
                
		JLabel lbl3 = new JLabel();
		lbl3.setBackground(Color.GRAY);
		lbl3.setLayout(null);

		Image img = new ImageIcon(this.getClass().getResource("key.png")).getImage();
		lbl3.setIcon(new ImageIcon(img));
		lbl3.setBounds(10, 20, 80, 80);
		jframe.add(lbl3);

		jframe.setVisible(true);

	}

	public static void main(String args[]) {

		Connect ct = new Connect();
		try {
			ct.server();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new Login();
	}

	private void loginnow() {
		try {

			String Uid = txt.getText();
			String Pass = String.valueOf(txt1.getPassword());
                        
                        if(Uid.length()==0||Pass.length()==0)
				 JOptionPane.showMessageDialog(null, "Fields cannot be empty.", "Error", JOptionPane.ERROR_MESSAGE);
			 else if (!TxtCompare.compare(Uid, "Aas")) {
			    JOptionPane.showMessageDialog(null, "Please enter a valid username", "Error", JOptionPane.ERROR_MESSAGE);
			    
			    }  
				else if (TxtCompare.compare(Pass,"pwd")) { 
			 
                                    JOptionPane.showMessageDialog(null, "Please enter a valid Password", "Error", JOptionPane.ERROR_MESSAGE);
                        }
		
                        
			String sql = "SELECT * FROM `login`WHERE username='" + Uid + "'";
			ResultSet rs = display(sql);
                        Pass = Encode.sha256(Pass);
			while (rs.next()) {
				if (rs.getString(1).equals(Uid) && rs.getString(2).equals(Pass)) {
					String userType = rs.getString(3);

					new Dashboard(userType);
					jframe.dispose();
				} else {
					JOptionPane.showMessageDialog(null, "Username or Password donot match");
				}
			}
		}

		catch (Exception e) {

		}

	}}