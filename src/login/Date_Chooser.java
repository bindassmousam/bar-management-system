package common_modules;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import static javax.swing.SwingConstants.CENTER;

/**
 *
 * @author Gaurav Nyaupane "NIGHTMARE"
 * @project Third_Eye
 * @package common_modules
 * @file Date_Chooser.java
 * 
 * @created Jun 27, 2019 11:39:53 PM
 * --+ COPYRIGHT PROTECTED MATERIAL +--
 *
 **/
public class Date_Chooser extends JDialog{
    public JTextField jTextField;
    JButton jButton1 = new JButton();
    JComboBox<Integer> jComboBox1 = new JComboBox<>();
    JComboBox<String> jComboBox2 = new JComboBox<>();
    JComboBox<Integer> jComboBox3 = new JComboBox<>();
    JLabel jLabel1 = new JLabel();
    String[] months = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    @SuppressWarnings({"unchecked", "Convert2Lambda"})
    public Date_Chooser(JTextField jTextField1){
        jTextField = jTextField1;
        setTitle("Date Chooser");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(256,137);
        setLayout(null);

        jLabel1.setText("Choose Date:");
        jLabel1.setHorizontalAlignment(CENTER);
        jLabel1.setVerticalAlignment(CENTER);
        jLabel1.setBounds(80, 12, 96, 15);

        jButton1.setText("<<");
        jButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                jButton1ActionPerformed();
            }
        });
        jButton1.setBounds(100, 70, 54, 25);

        for(int i = 1;i<=31;i++)
            jComboBox1.addItem(i);
        jComboBox1.setBounds(12, 33, 61, 24);
        jComboBox2.setModel(new DefaultComboBoxModel<>(months));
        jComboBox2.setBounds(91, 33, 63, 24);

        for(int i = 1900;i<=2019;i++)
            jComboBox3.addItem(i);
        jComboBox3.setBounds(172, 33, 71, 24);
        
        add(jLabel1);
        add(jButton1);
        add(jComboBox1);
        add(jComboBox2);
        add(jComboBox3);
        
        setLocationRelativeTo(null);
        setVisible(true);
    }                      
    @SuppressWarnings("RedundantStringConstructorCall")
    private void jButton1ActionPerformed() {
        @SuppressWarnings("RedundantStringToString")
        String day = new String(jComboBox1.getSelectedItem().toString());
        @SuppressWarnings("RedundantStringToString")
        String month = new String(jComboBox2.getSelectedItem().toString());
        int mm = 0;
        for(int c=0;c<months.length;c++)
            if(months[c].equals(month)){
                mm = c+1;
                break;
            }
        @SuppressWarnings("RedundantStringToString")
        String year = new String(jComboBox3.getSelectedItem().toString());
        String date = new String(year+"-"+mm+"-"+day);
        jTextField.setText(date);
        dispose();
    }
}