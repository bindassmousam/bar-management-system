/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;


public class reports {
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public reports(){
        JFrame jframe= new JFrame("Generate Report");
        jframe.setSize(250,120);
        jframe.setLocationRelativeTo(null);
        jframe.setLayout(null);

        jframe.setResizable(false);
        JButton btn3=new JButton("Purchase Report");
        btn3.addActionListener((ActionEvent e) -> {
            new PurchaseReport();
        });
        btn3.setBounds(25,15,200,30);
        jframe.add(btn3);

        JButton btn=new JButton("Sales Report");
        btn.addActionListener((ActionEvent e) -> {
            new SalesReport();
        });
        btn.setBounds(25,50,200,30);
        jframe.add(btn);
        jframe.setVisible(true);
    }
}
