package login;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class Connect {

	String driver="com.mysql.cj.jdbc.Driver";
    String url="jdbc:mysql://localhost:3306/bms";
    String un="root";
    String password="";
    public static Connection conn;
    public static Statement s;
    
    
    void server() throws Exception{
    //code to run XAMPP server before jar file execution and connection to server    
    	    try{
    	    
    	        Runtime runtime = Runtime.getRuntime();     //getting Runtime object
    	 
    	        Process process = runtime.exec("C:\\xampp\\xampp-control.exe");     //open xampp
    	 
    	        Thread.sleep(2000);
    	 
    	        process.destroy();
    	    }catch(Exception e){
    	    	
    	    }
    	  }
    		
    
    
    public Connection getConnect() {
    	 
			
		try{
			Class.forName(driver);
			conn = DriverManager.getConnection(url, un,password); 
	        s = conn.createStatement();
	     }
		catch(Exception e ){
			JOptionPane.showMessageDialog(null,e.getMessage());
		}
            return conn;
	
	}
	public void append(String sql,String msg){
		try{
			getConnect();
			s.execute(sql);
                        if(msg!=null)
                            JOptionPane.showMessageDialog(null,"Data "+msg+" Successfully" );
		}
		catch (Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	}
	
	public ResultSet display(String sql){
		ResultSet rs=null;
		try {
			getConnect();
			rs=s.executeQuery(sql);
		} catch (SQLException e) {
                    JOptionPane.showMessageDialog(null,e.getMessage());

		}
		return rs;
		
	}
        public int exec(String sql,int i){
            int flag=0;
            try {
                getConnect();
                flag=s.executeUpdate(sql,i);
            } catch (SQLException e) {
            }
            return flag;	
	}
}
