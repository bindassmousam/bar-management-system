package supplier;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import login.Connect;

public class del_supp extends Connect{

	public del_supp(final String ID) {
		final JFrame del=new JFrame("Delete Supplier");
		del.setSize(350,150);
		del.setLayout(null);
		del.setLocationRelativeTo(null);
		
		JLabel ask=new JLabel("Do you want to delete Supplier ID: " + ID );
		ask.setBounds(1,10,330,50);
		ask.setHorizontalAlignment(JLabel.CENTER);
		del.getContentPane().add(ask, BorderLayout.CENTER);
		
		JButton yes=new JButton("YES");
		yes.setBounds(60,70,100,30);
		del.add(yes);
		
		JButton no=new JButton("NO");
		no.setBounds(180,70,100,30);
		del.add(no);
		
		yes.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
					String sql="DELETE FROM `supplier` WHERE `supplier`.`Supplier_id` = '"+ID+"'";
					append(sql,"Deleted");
					del.dispose();
					
			}

		});
		
		no.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				del.dispose();
				
			}
			});
		
		
		
		
		del.setVisible(true);

}

}
