package Employee;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public final class eview extends login.Connect {

    public eview() {
        JScrollPane sp;
        JFrame cview = new JFrame("Employee details");

        String column[] = {"Emp_Id", "Name", "Address", "Gender", "Phone No", "Post"};
        final DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(column);

        JTable jt = new JTable();
        jt.setModel(model);
        sp = new JScrollPane(jt);
        sp.setBounds(40, 50, 770, 200);
        String sql = "SELECT * FROM `employee` ";
        readdata(jt,sp, model, sql);
        cview.add(sp);

        cview.setSize(850, 400);

        final JLabel searchL = new JLabel("Search :");
        searchL.setBounds(10, 10, 50, 20);
        cview.add(searchL);

        final JTextField searchT = new JTextField();
        searchT.setBounds(60, 10, 300, 20);
        cview.add(searchT);
        searchT.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                String sql = "SELECT * FROM `employee` WHERE `Name` LIKE '%" + searchT.getText().toString() + "%' || `Emp_Id` LIKE '%" + searchT.getText().toString() + "%'";
                readdata(jt,sp, model, sql);
            }
        });

        JButton abtn = new JButton("Add New");
        abtn.setBounds(440, 280, 100, 30);
        cview.add(abtn);
        abtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                cview.dispose();
                new new_employee();
            }
        });

//        JButton rbtn = new JButton("Refresh");
//        rbtn.setBounds(640, 280, 100, 30);
//        cview.add(rbtn);
//        rbtn.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent ae) {
//
//                String sql = "SELECT * FROM `employee`";
//                readdata(jt,sp, model, sql);
//
//            }
//        });

        JButton clear = new JButton("CLEAR");
        clear.setBounds(140, 170, 80, 30);
        cview.add(clear);

        JButton ubtn = new JButton("Update");
        ubtn.setBounds(200, 280, 100, 30);
        cview.add(ubtn);
        ubtn.addActionListener(new ActionListener() {
            String ID;

            public void actionPerformed(ActionEvent ae) {

                int[] row = jt.getSelectedRows();
                if(row.length<=0){
                                                                JOptionPane.showMessageDialog(null, "Please Select a employee.");
                                                                return;
                                                            }

                for (int i = 0; i < row.length; i++) {
                    ID = (String) jt.getValueAt(row[i], 0);

                }
                cview.dispose();

                new up_emp(ID);
            }

        });

        JButton dbtn = new JButton("Delete");
        dbtn.setBounds(320, 280, 100, 30);
        cview.add(dbtn);
        dbtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {

                int[] row = jt.getSelectedRows();
                 if(row.length<=0){
                                                                JOptionPane.showMessageDialog(null, "Please Select a employee.");
                                                                return;
                                                            }
                String ID = null;
                for (int i = 0; i < row.length; i++) {
                    ID = (String) jt.getValueAt(row[i], 0);

                }
                                cview.dispose();

                new del_emp(ID);

            }
        });

        cview.setLocationRelativeTo(null);
        cview.setLayout(null);
        cview.setVisible(true);

//		 		try {
//		 			String sql= "select * from customer";
//		 			ResultSet rs= display(sql);
//		 			while(rs.next()) {
//		 				model.addRow(new String[] {rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6)});
//		 			}
//		 			
//			 		}catch(Exception e) {
//	 			
//			 		}
        cview.addWindowListener(new WindowAdapter() {

            @Override
            public void windowActivated(WindowEvent arg0) {
                String sql = "SELECT * FROM `employee`";
                readdata(jt,sp, model, sql);

            }
        });

    }
    public void readdata(JTable jt,JScrollPane sp, DefaultTableModel model, String sql) {
         sp.repaint();
        model.getDataVector().removeAllElements();
       

        ResultSet rs = display(sql);
        try {
            while (rs.next()) {
                model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)});
            }
        } catch (SQLException ex) {
            Logger.getLogger(eview.class.getName()).log(Level.SEVERE, null, ex);
        }
        jt.clearSelection();
    }
}
